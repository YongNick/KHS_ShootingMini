﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShooting : MonoBehaviour {

    // 적 총알 prefeb을 담기 위한 gameobject
    public GameObject Bullet;
    // 다음 슈팅까지 대기시간.
    public float ShootingWait = 3.0f;

    // 총알 생성용 코루틴
    IEnumerator Shoot()
    {
        while (true)
        {
            Instantiate(Bullet,transform.position,transform.rotation);
            yield return new WaitForSeconds(ShootingWait);
        }
    }
	// Use this for initialization
	void Start () {
        // 코루틴 실행
        StartCoroutine("Shoot");
	}
	
}
