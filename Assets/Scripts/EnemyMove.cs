﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMove : MonoBehaviour {


    // 적 이동지점을 저장해둘 배열
    public Vector3[] movePoint; 

    // 적이 생성된 후, 대기시간이 필요할 경우 사용할 변수
    public float AwakeTime = 0;
    // 적 속도
    public float Speed = 3;
    // 한 지점 이동 후, 대기시간이 필요할 경우 사용할 변수
    public float WaitTime = 0;

    // 이동 후 다음 지점을 정하기 위한 변수
    int go = 2;
    // 이동이 끝나서 다시 반대방향으로 돌아가야하는지 판단할 변수
    bool back = false;

    // 적들은 계속 플레이어를 주시하면서 총알을 쏴줘야하기 때문에, 추가한 플레이어 GameObject
    GameObject PlayerObj;

    // 총 이동지점들을 저장할 Vector3배열
    Vector3[] Pos;

    // 적이 생성된 시작지점.
    Vector3 firstPos;
    
	// Use this for initialization
	void Start () 
    {
        // 시작지점 저장
        firstPos = transform.position;
        // 플레이어 오브젝트 불러옴
        PlayerObj = GameObject.Find("Player");
        // 이동지점들은 적이 이동할 지점들과, 적이 생성된 지점까지 포함해야하므로, Length가 1 더 길어야 함.
        Pos = new Vector3[movePoint.Length + 1];
        // 초기지점 저장
        Pos[0] = firstPos;

        // 나머지 이동지점들 저장
        for (int i = 1; i <= movePoint.Length; i++) 
        {
            Pos[i] = Pos[i - 1] + transform.TransformDirection(movePoint[i - 1]);
        }
        // 이동 코루틴 실행
        StartCoroutine("Move");
	}
	
	IEnumerator Move()
    {
        yield return new WaitForSeconds(AwakeTime);
        while(true)
        {
            //적이 계속 플레이어을 보도록 설정.
            transform.LookAt(PlayerObj.transform);
            // 적이 다음 이동지점에 도착했을 경우,
            if(transform.position == Pos[go])
            {
                // 다음 이동을 뒤로 이행할것인지 아닌지부터 확인
                if(!back)
                {
                    // 다음 go(이동지점)이 Pos(이동지점들의 합)의 길이보다 클경우,
                    if(++go == Pos.Length)
                    {
                        // 뒤로 돌릴수 있도록 설정,
                        back = true;
                        // ++go 해주었기때문에 -2
                        go = go - 2;
                    }
                }
                else
                {
                    // 마찬가지로 다음 이동지점이 0일 경우,
                    if (--go == 0)
                    {
                        // 원방향으로 갈 수 있도록 설정.
                        back = false;
                        // --go이기때문에 +2
                        go = go + 2;
                    }
                }
                // 이동 완료후 대기시간이 필요한 경우 대기.
                yield return new WaitForSeconds(WaitTime);
            }
            else
            {
                // 적이 계속 다음지점으로 이동.
                transform.position = Vector3.MoveTowards(transform.position, Pos[go], Speed * Time.deltaTime);
                // 코루틴 최적화를 위한 WaitForFixedUpdate
                yield return new WaitForFixedUpdate();
            }
           
        }
    }


	
}
