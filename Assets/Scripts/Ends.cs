﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ends : MonoBehaviour {
    
    // 플레이어가 움직일 수 있는 방향을 설정해주는 것들이 End들(Top,Bottom,Right,Left)이므로, 
    // 플레이어의 움직임에 따라 같이 움직여 줄 수 있도록 하기 위해 Player의 Transform을 받아오기 위해 만든 변수.
    Transform Player;

    // 또한 Ends(하이어라키상의 Parent)에는 플레이어가 마우스 방향을 바라볼 수 있도록 도와주는
    // Quad가 포함되어있음(RayCast)

	// Use this for initialization
	void Start () 
    {
        //Player의 Transform을 받아옴.
        Player = GameObject.Find("Player").transform;
	}
	
	// Update is called once per frame
	void Update () 
    {
        // 플레이어를 계속 따라가줄 수 있게 함.
        transform.position = Player.position;
	}
}
