﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineRend : MonoBehaviour {


    // 원을 그리기 위해 쪼개줄 segments.
    public int segments = 40;
    // 원의 반지름.
    public float xRadi = 10;
    public float yRadi = 10;

    //LineRenderer
    LineRenderer LRend;

	// Use this for initialization
	void Start () 
    {
        // 원을 그리기위해 설정함.
        LRend = GetComponent<LineRenderer>();
        // Vertex수를 segments 수만큼 쪼갤수 있도록 설정.
        LRend.SetVertexCount(segments+1);
        // 색 설정.
        LRend.SetColors(new Color(255,255,255),new Color(255,255,255));
        // 원을 그리기 위한 함수 실행
        CreatePoints();
	}


    void CreatePoints()
    {
        // 원을 2D처럼 그릴 것이기 때문에, z축은 0으로 설정해둠.
        float x;
        float y;
        float z = 0f;

        // 삼각함수를 이용해 원을 그릴 것이기 때문에, 필요한 angle 변수
        float Angle = 20f;
        
        for(int i=0; i<(segments +1);i++)
        {
            // x,y의 반지름값에 따라 다음 원을 그릴 지점을 지정
            x = Mathf.Cos(Mathf.Deg2Rad * Angle) * xRadi;
            y = Mathf.Sin(Mathf.Deg2Rad * Angle) * yRadi;

            LRend.SetPosition(i, new Vector3(x, y, z));
            // angle은 다음 지점을 정하기 위해, 360을 segments, 쪼개진 값 만큼으로 나눈 것을 더한다. 원이 그려질 때까지 반복.
            Angle += (360 / segments);
        }
    }

}
