﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHP : MonoBehaviour {

    Spawn Enemy;
    // 적의 HP를 설정할 수 있도록 public으로 변수 설정.
    public int EnemyHPCount = 3;
    void Start()
    {
        Enemy = GameObject.Find("EnemySpawner").GetComponent<Spawn>();
    }

    // 다른 스크립트에서 함수 실행을 시킬 수 있도록 public으로 설정.
    public void EnemyHPDown()
    {
        // 적 HP에 따라 삭제, 혹은 HP를 낮춤.
        if (EnemyHPCount > 0)
        {
            EnemyHPCount--;
            if (EnemyHPCount <= 0)
            {
                // 적 개체수 줄임.
                Enemy.enemyCountDown();
                Destroy(gameObject);
            }
        }// 혹시 모를 버그를 위해.
        else
        {
            Enemy.enemyCountDown();
            Destroy(gameObject);
        }
    }
}
