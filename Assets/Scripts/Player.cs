﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour {

    // 플레이어 Hp
    public int MaxPlayerHP = 4;
    // 플레이어의 스킬 쿨타임
    public float SkillCoolDown = 2.0f;    
    // 플레이어 속도
    public float Speed = 5;
    // 플레이어 총알
    public GameObject PreBullet;
    public Image PlayerHPImage;

    // 플레이어가 움직일 수 있는 공간의 기준이 되어 줄 GameObject
    GameObject Standard;
    // 플레이어가 움직일 수 있는 방향을 정해줄 GameObject들
    GameObject Left;
    GameObject Right;
    GameObject Top;
    GameObject Bottom;

    // 플레이어 Transform
    Transform PTransform;

    // 플레이어가 바라볼 지점을 저장할 Vector3
    Vector3 targetpos;

    // 지금 화면이 멈추었는지 확인해줄 변수
    bool isPause = false;
    // 현재 스킬이 쿨타임인지 확인해줄 변수
    bool isSkillCoolDown = false;


    // Raymask
    int RayMask;
    // 현재 플레이어HP
    int playerHP;

	// Use this for initialization
	void Start () 
    {
        //초기 플레이어Hp설정
        playerHP = MaxPlayerHP;
        // 필요한 값들을 불러와 저장해줌.
        Standard = GameObject.Find("Standard");
        Left = GameObject.Find("LeftEnd");
        Right = GameObject.Find("RightEnd");
        Top = GameObject.Find("TopEnd");
        Bottom = GameObject.Find("BottomEnd");
		PTransform = GetComponent<Transform>();

        // 플레이어가 바라볼 방향을 지정하기위해 Raycast를 사용할 것인데,
        // 그를 위해 Quad의 Layer를 저장해줌.
        RayMask = (1 << LayerMask.NameToLayer("PointerLayer"));
	}
	
	// Update is called once per frame
    void Update()
    {
        // Pause상태(메뉴버튼을 누른 상태)가 아닐때만 움직이도록 설정.
        if (!isPause)
        {
            Moving();
            PlayerLook();
            Shooting();
        }
    }

    void PlayerLook()
    {
        // 카메라를 기준으로, 마우스가 있는 지점으로 Ray 생성.
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        // Raycast를 위한 RayCastHit변수
        RaycastHit hit;
        // Raycast, RayMask에 설정된 레이어에만 충돌.
        if (Physics.Raycast(ray, out hit, 1000f, RayMask))
        {
            // 플레이어가 볼 방향 설정
            targetpos = hit.point;
        }
        // 보도록 설정
        PTransform.LookAt(targetpos);
    }

    void Moving()
    {
        // 기준으로부터 30만큼의 거리가 나지 않을 경우에만 움직이도록 설정.
        if (Vector3.Distance(PTransform.position, Standard.transform.position) < 30)
        {
            if (Input.GetKey(KeyCode.D))
            {
                PTransform.position = Vector3.MoveTowards(PTransform.position, Right.transform.position, Speed * Time.deltaTime);
            }
            if (Input.GetKey(KeyCode.A))
            {
                PTransform.position = Vector3.MoveTowards(PTransform.position, Left.transform.position, Speed * Time.deltaTime);
            }
            if (Input.GetKey(KeyCode.W))
            {
                PTransform.position = Vector3.MoveTowards(PTransform.position, Top.transform.position, Speed * Time.deltaTime);
            }
            if (Input.GetKey(KeyCode.S))
            {
                PTransform.position = Vector3.MoveTowards(PTransform.position, Bottom.transform.position, Speed * Time.deltaTime);
            }
        }
        // 기준과 30이상의 거리차이가 날 경우, 살짝 끌어와줌.
        else
        {
            PTransform.position = Vector3.MoveTowards(PTransform.position, Standard.transform.position, 1 * Time.deltaTime);
        }
    }

    void Shooting()
    {
        // 쏠 지점을 저장하기 위한 TempVector
        Vector3 TempVec = new Vector3(0, 0, 0);
        if (Input.GetMouseButtonDown(0))
        {
            // Transform으로부터 z축으로 2만큼의 거리 앞에 생성할 수 있도록 위치지정.
            TempVec = transform.position + transform.TransformDirection(new Vector3(0, 0, 2));
            // 총알 생성.
            Instantiate(PreBullet, TempVec, transform.rotation);
        }
        else if(Input.GetKeyDown(KeyCode.Space))
        {
            // Space키를 눌렀을 때, 스킬을 발동할 수 있도록 코루틴 실행.
            StartCoroutine("SpaceSkill");
        }
    }

    IEnumerator SpaceSkill()
    {
        // 스킬 발동 시 나가게 될 총알 개수 설정.
        int skillcount = 5;
        // 총알 개수를 세기 위한 변수
        int count = 0;
        // 왼쪽과 오른쪽에서 나가게 할 것이기 때문에, 교차로 나가게 하기 위한 bool 변수
        bool LeftRight = false;
        // 총알이 나가게 하기 위한 위치 변수
        Vector3 TempVec = new Vector3(0,0,0);
        // 스킬 쿨타임이 아닐 경우에만 실행
        if (!isSkillCoolDown)
        {
            // 스킬이 쿨타임임을 설정.
            isSkillCoolDown = true;
            // 쿨타임 코루틴 실행
            StartCoroutine("SkillCooldownRoutine");
            // 5발만 나갈 것이기 때문에, while로 실행
            while (count < skillcount)
            {
                // 왼쪽 오른쪽을 구분해서 나가도록 설정
                if (LeftRight)
                {
                    TempVec = transform.position + transform.TransformDirection(new Vector3(0, 2.5f, 0));
                }
                else if (!LeftRight)
                {
                    TempVec = transform.position + transform.TransformDirection(new Vector3(0, -2.5f, 0));
                }
                // 총알 생성
                Instantiate(PreBullet, TempVec, transform.rotation);
                // 0.2초의 간격으로 나가도록 설정
                yield return new WaitForSeconds(.2f);
                // 왼쪽 오른쪽을 번갈아 나가게 하기 위해 설정
                if (LeftRight)
                {
                    LeftRight = false;
                }
                else if (!LeftRight)
                {
                    LeftRight = true;
                }
                // 총알 개수 ++;
                count++;
            }
        }
        else 
        {
            //쿨타임
            Debug.Log("Skill is now Cooltime");
        }
    }

    // 쿨타임 루틴
    IEnumerator SkillCooldownRoutine()
    {
        yield return new WaitForSeconds(SkillCoolDown);
        isSkillCoolDown = false;
    }

    // 플레이어 Hp를 줄이는 함수.
    public void PlayerHPDown()
    {
        if (--playerHP != 0)
        {
            // HP 표기를 위해 fillamount를 HP퍼센트만큼 설정.
            float hpPer = (float)playerHP / (float)MaxPlayerHP;
            PlayerHPImage.fillAmount = hpPer;
        }
        else
        {
            SceneManager.LoadScene(0);
        }
    }

    // 게임 정지 관련 함수.
    public void Pause()
    {
        isPause = true;
    }

    public void Restart()
    {
        isPause = false;
    }
}
