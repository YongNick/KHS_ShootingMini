﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Spawn : MonoBehaviour {

    // 적 최대수.
    public int MaxEnemyCount = 5;
    // 적 Object Prefeb을 받아오기 위한 GameObject
    public GameObject EnemyObj;
    // 적의 위치정보를 임시적으로 저장,비교하기 위한 GameObject
    public GameObject TempObj;
    // 적 생성 대기시간
    public float SpawnWait = 10;
    
    // 플레이어와의 위치를 비교해야하기 때문에 플레이어를 받아올 GameObject
    GameObject Player;


    // 적 위치를 저장할 Vector3
    Vector3 EnemyPos;
    // 플레이어 위치를 저장할 Vector3
    Vector3 PlayerPos;
    // 적 Quaternion 저장,비교용 Temp
    Quaternion TempQ;


    int EnemyCount = 0;
    // 적 생성용 코루틴
    IEnumerator SpawnEnem()
    {
        // 적의 수를 세기 위한 변수
        int enemyCount = 0;
        // 최대수만큼 생성되도록 설정
        while (enemyCount < MaxEnemyCount)
        {
            // 적 위치를 설정할 함수 실행
            EnemySpawnPos();
            // 적 생성 함수 실행
            EnemySpawn();
            // 카운트++
            enemyCount++;
            // 대기시간만큼 대기
            yield return new WaitForSeconds(SpawnWait);
        }
    }

	// Use this for initialization
	void Start () 
    {
        // 플레이어 오브젝트 불러옴
        Player = GameObject.Find("Player");
        // 플레이어의 위치 저장
        PlayerPos = Player.transform.position;
        // 적 수 저장
        EnemyCount = MaxEnemyCount;
        // 적 생성 코루틴 실행
        StartCoroutine("SpawnEnem");
	}
	
    // 생성될 적의 위치를 설정할 함수.
    void EnemySpawnPos()
    {
        while(true)
        {
            // 원 밖에 나가지 않도록 랜덤한 위치에 생성.
            int enemyX = Random.Range(-20, 20);
            int enemyY = Random.Range(-20, 20);

            EnemyPos = new Vector3(enemyX, enemyY, 0);

            // 임시 오브젝트에 적의 위치를 저장.
            TempObj.transform.position = EnemyPos;
            // 임시 오브젝트가 플레이어를 바라보도록 설정.
            TempObj.transform.LookAt(Player.transform);
            // 임시 쿼터니언에 임시오브젝트 쿼터니언 저장
            TempQ = TempObj.transform.rotation;
            // y축이 0으로 돌아가게 되버리면, Moving 함수 콜이 이상하게 꼬이는 버그때문에 잠시 락을 걸어둠
            if(TempQ.eulerAngles.y == 0)
            {
                continue;
            }

            // 플레이어와의 거리가 10 이상일 경우에만 생성되도록 설정
            if(Vector3.Distance(PlayerPos,EnemyPos) < 10)
            {
                continue;
            }
            else
            {
                break;
            }
        }
    }

    void EnemySpawn()
    {
        // 저장된 위치값과 쿼터니언으로 적 오브젝트 생성.
        Instantiate(EnemyObj, EnemyPos, TempQ);
    }

    // 적 수가 다 닳았을 경우, 다시 시작점으로.
    public void enemyCountDown()
    {
        if(--EnemyCount == 0)
        {
            SceneManager.LoadScene(0);
        }
    }
}
