﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class Buttonamage : MonoBehaviour {


    // 메뉴 패널
    public GameObject CancelPanel;

    // 플레이어의 동작을 멈추기 위한 GameObject 변수
    GameObject PlayerObj;

    // 메뉴버튼의 Button Component를 불러주기 위한 변수
    Button CancelbuttonObj;

    void Start()
    {
        // 플레이어와 메뉴버튼을 각자 변수에 넣어줌.
        PlayerObj = GameObject.Find("Player");
        CancelbuttonObj = GameObject.Find("CancelButton").GetComponent<Button>();
    }

    // 메뉴버튼 함수
    public void CancelButton()
    {
        // 플레이어의 슈팅,움직임,마우스를 따라 보는것을 멈추기 위해 함수 작동.
        PlayerObj.GetComponent<Player>().Pause();
        // 메뉴버튼이 다시 눌리지 않도록 설정
        CancelbuttonObj.interactable = false;
        // 메뉴 패널을 활성화
        CancelPanel.SetActive(true);
        // 게임 화면의 시간을 멈춤.
        Time.timeScale = 0.0f;
    }

    // 게임 재개 및 X 버튼 함수
    public void Restart()
    {
        // 다시 플레이어가 움직일 수 있도록 함수 작동
        PlayerObj.GetComponent<Player>().Restart();
        // 메뉴버튼을 다시 누를 수 있도록 설정
        CancelbuttonObj.interactable = true;
        // 패널을 다시 투명화시킴.
        CancelPanel.SetActive(false);
        // 게임 화면의 시간을 다시 흘러가게 설정.
        Time.timeScale = 1.0f;
    }
    public void GoToStartScene()
    {
        // 시작 화면, 빌드세팅에서 0번으로 설정해두었음.
        SceneManager.LoadScene(0);
    }
    public void QuitGame()
    {
        // 종료
        Application.Quit();
    }
}
