﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {


    // 수정을 용이하게 하기 위한 public 변수
    public float BulletLifeTime = 2.5f;
    public float BulletSpeed = 25;

    // 플레이어와 적의 스크립트의 함수를 불러주기 위한 변수
    EnemyHP EnemyHPScr;
    Player PlayerScr;

    // 피아구분을 하기 위한 bool 변수
    bool isPlayerB = false;
    bool isEnemyB = false;

    // 총알들이 일정 기간을 두고 삭제되게 하기 위한 루틴
    IEnumerator DeleteTimer()
    {
        yield return new WaitForSeconds(BulletLifeTime);
        Destroy(gameObject);
    }


    void Start()
    {
        // Tag에 따라 플레이어의 총알인지, 적의 총알인지 구분
        if (transform.CompareTag("PlayerB"))
        {
            isPlayerB = true;
        }
        if (transform.CompareTag("EnemyB"))
        {
            isEnemyB = true;
        }
        // 삭제타이머 작동
        StartCoroutine(DeleteTimer());
	}
	
	// Update is called once per frame
	void Update () 
    {
        transform.Translate(new Vector3(0, 0, BulletSpeed) * Time.deltaTime);
	}

    void OnTriggerEnter(Collider col)
    {
        // 총알이 플레이어의 것인지, 적의 것인지 구분 후,
        if (isPlayerB)
        {
            // 닿은 Trigger가 적일 경우, 적의 HP를 줄이는 함수 호출 후 총알 소멸
            if (col.CompareTag("EnemyC"))
            {
                EnemyHPScr = col.GetComponent<EnemyHP>();
                EnemyHPScr.EnemyHPDown();
                Destroy(gameObject);
            }
        }
        else if (isEnemyB)
        {
            // 닿은 Trigger가 플레이어일 경우, 플레이어 Hp를 줄이는 함수 호출 후 소멸
            if (col.CompareTag("PlayerC"))
            {
                PlayerScr = col.GetComponent<Player>();
                PlayerScr.PlayerHPDown();
                Destroy(gameObject);
            }
        }
    }
}
